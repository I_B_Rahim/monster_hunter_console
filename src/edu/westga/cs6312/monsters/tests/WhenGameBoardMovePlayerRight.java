package edu.westga.cs6312.monsters.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6312.monsters.model.GameBoard;

/**
 * This is to test the Game board moveRight method of the GameBoard class
 * 
 * @author Ibrahim Tonifarah
 * @version 2/15/2019
 *
 */
class WhenGameBoardMovePlayerRight {

	/**
	 * test moveRight method to return "Room at (1)"
	 */
	@Test
	public void testGameBoardMoveRightFrom0To1() {
		GameBoard startGameBoard = new GameBoard();
		startGameBoard.moveRight();
		String result = startGameBoard.getCurrentRoom();
		assertEquals("Room at (1)", result);
	}

	/**
	 * test moveRight method to return "Room at (0)"
	 */
	@Test
	void testGameBoardMoveRightFrom9To0() {
		GameBoard startGameBoard = new GameBoard();
		for (int count = 0; count < 10; count++) {
			startGameBoard.moveRight();
		}		
		String result = startGameBoard.getCurrentRoom();
		assertEquals("Room at (0)", result);
	}
}
