
package edu.westga.cs6312.monsters.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6312.monsters.model.KickingMonster;

/**
 * This is to test the getHealthCredit method of the Kicking monster class
 * 
 * @author Ibrahim Tonifarah
 * @version 2/14/2019
 *
 */
class WhenKickingMonsterGetHealthCredits {

	/**
	 * test getHealthCredit method produce 100 health credits
	 */
	@Test
	void testKickingMonsterGetHealthCreditsReturns100() {
		KickingMonster theKickingMonster = new KickingMonster();
		int result = theKickingMonster.getHealthCredits();
		assertEquals(100, result);
	}
	
	/**
	 * test setHealthCredit method with 25 returns 25 health credits
	 */
	@Test
	void testKickingMonsterSetHealthCreditsWith25() {
		KickingMonster theKickingMonster = new KickingMonster();
		theKickingMonster.setHealthCredits(25);
		int result = theKickingMonster.getHealthCredits();
		assertEquals(25, result);
	}

}
