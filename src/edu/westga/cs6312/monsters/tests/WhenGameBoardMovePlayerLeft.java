
package edu.westga.cs6312.monsters.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6312.monsters.model.GameBoard;

/**
 * This is to test the Game board moveLeft method of the GameBoard class
 * 
 * @author Ibrahim Tonifarah
 * @version 2/15/2019
 *
 */
class WhenGameBoardMovePlayerLeft {

	/**
	 * test moveLeft method to return "Room at (9)"
	 */
	@Test
	public void testGameBoardMoveLeftFrom0To9() {
		GameBoard startGameBoard = new GameBoard();
		startGameBoard.moveLeft();
		String result = startGameBoard.getCurrentRoom();
		assertEquals("Room at (9)", result);
	}

	/**
	 * test moveLeft method to return "Room at (1)"
	 */
	@Test
	void testGameBoardMoveLeftFrom0To1() {
		GameBoard startGameBoard = new GameBoard();
		for (int count = 0; count < 9; count++) {
			startGameBoard.moveLeft();
		}		
		String result = startGameBoard.getCurrentRoom();
		assertEquals("Room at (1)", result);
	}
}
