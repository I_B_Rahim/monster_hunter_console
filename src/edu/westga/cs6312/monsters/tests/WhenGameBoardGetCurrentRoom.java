
package edu.westga.cs6312.monsters.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6312.monsters.model.GameBoard;

/**
 * This is to test to confirm that the GameBoard getCurrentRoom method functions correctly
 * 
 * @author Ibrahim Tonifarah
 * @version 2/14/2019
 *
 */
class WhenGameBoardGetCurrentRoom {

	/**
	 * test GameBoard getCurrentRoom returns a room at location 0
	 */
	@Test
	public void testGetCurrentRoomIsAt0() {
		GameBoard theGameBoard = new GameBoard();
		String result = theGameBoard.getCurrentRoom();
		assertEquals("Room at (0)", result);
	}

}
