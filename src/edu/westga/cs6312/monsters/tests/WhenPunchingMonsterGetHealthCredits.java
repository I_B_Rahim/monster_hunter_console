
package edu.westga.cs6312.monsters.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6312.monsters.model.PunchingMonster;

/**
 * This is to test the getHelthCredits method of the Punching monster class
 * 
 * @author Ibrahim Tonifarah
 * @version 2/16/2019
 *
 */
class WhenPunchingMonsterGetHealthCredits {

	/**
	 * test getHealthCredits method produce to see if 75 is returned
	 */
	@Test
	void testPunchingMonsterGetHealthCreditsIs75() {
		PunchingMonster thePunchMonster = new PunchingMonster();
		int result = thePunchMonster.getHealthCredits();
		assertEquals(75, result);
	}
	
	/**
	 * test setHealthCredits method with 3 returns 3
	 */
	@Test
	void testPunchingMonsterSetHealthCreditsWith3() {
		PunchingMonster thePunchMonster = new PunchingMonster();
		thePunchMonster.setHealthCredits(3);
		int result = thePunchMonster.getHealthCredits();
		assertEquals(3, result);
	}

}
