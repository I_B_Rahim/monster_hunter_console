
package edu.westga.cs6312.monsters.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6312.monsters.model.KickingMonster;

/**
 * This is to test the constructor and toString method of the Kicking monster class
 * 
 * @author Ibrahim Tonifarah
 * @version 2/14/2019
 *
 */
class WhenKickingMonsterIsCreated {

	/**
	 * test constructor and toString method produce accurate results
	 */
	@Test
	void testKickingMonsterConstructorAndToString() {
		KickingMonster theKickingMonster = new KickingMonster();
		String result = theKickingMonster.toString();
		assertEquals("Kicking Monster with 100 health credits", result);
	}
}
