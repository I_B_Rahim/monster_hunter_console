
package edu.westga.cs6312.monsters.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6312.monsters.model.Room;

/**
 * This is to test to confirm that the Room class getLocation method functions correctly
 * 
 * @author Ibrahim Tonifarah
 * @version 2/14/2019
 *
 */
class WhenRoomGetLocation {

	/**
	 * test the getLocation method with 2 as room Id number returns "Room at (2)"
	 */
	@Test
	public void testRoomGetLocationWith2() {
		Room theRoom = new Room(2);
		String result = theRoom.getLocation();
		assertEquals("Room at (2)", result);
	}
	
	/**
	 * test the getLocation method with 0 as room Id number returns "Room at (0)"
	 */
	@Test
	public void testRoomGetLocationWith0() {
		Room theRoom = new Room(0);
		String result = theRoom.getLocation();
		assertEquals("Room at (0)", result);
	}

	/**
	 * test the getLocation method with 9 as room Id number returns "Room at (9)"
	 */
	@Test
	public void testRoomGetLocationWith9() {
		Room theRoom = new Room(9);
		String result = theRoom.getLocation();
		assertEquals("Room at (9)", result);
	}
}
