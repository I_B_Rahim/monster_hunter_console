
package edu.westga.cs6312.monsters.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6312.monsters.model.Player;

/**
 * This is to test the constructor and toString method of the Player class
 * 
 * @author Ibrahim Tonifarah
 * @version 2/14/2019
 *
 */
class WhenPlayerIsCreated {

	/**
	 * test constructor and toString method produce accurate results
	 */
	@Test
	public void testPlayerConstructorAndToString() {
		Player thePlayer = new Player();
		assertEquals("The player has 100 health credits", thePlayer.toString());
	}

}
