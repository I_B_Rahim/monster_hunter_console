
package edu.westga.cs6312.monsters.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6312.monsters.model.KickingMonster;

/**
 * This is to test the fight method of the Kicking monster class
 * 
 * @author Ibrahim Tonifarah
 * @version 2/14/2019
 *
 */
class WhenKickingMonsterFights {

	/**
	 * test fight method produces 30 inflicted damage
	 */
	@Test
	void testKickingMonsterFight30Damage() {
		KickingMonster theKickingMonster = new KickingMonster();
		int result = theKickingMonster.fight();
		assertEquals(30, result);
	}

}
