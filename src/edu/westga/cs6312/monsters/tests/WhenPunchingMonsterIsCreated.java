
package edu.westga.cs6312.monsters.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6312.monsters.model.PunchingMonster;

/**
 * This is to test the constructor and toString method of the Punching monster class
 * 
 * @author Ibrahim Tonifarah
 * @version 2/15/2019
 *
 */
class WhenPunchingMonsterIsCreated {

	/**
	 * test constructor and toString method produce accurate results
	 */
	@Test
	void testPunchingMonsterConstructorAndToString() {
		PunchingMonster thePunchingMonster = new PunchingMonster();
		String result = thePunchingMonster.toString();
		assertEquals("Punching Monster with 75 health credits", result);
	}

}
