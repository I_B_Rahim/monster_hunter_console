
package edu.westga.cs6312.monsters.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6312.monsters.model.Player;

/**
 * This is to test getHealthCredits method of the Player class
 * 
 * @author Ibrahim Tonifarah
 * @version 2/14/2019
 *
 */
class WhenPlayerGetHealthCredits {

	/**
	 * test getHealthCredit method returns 100
	 */
	@Test
	public void testPlayerGetHealthCreditsIs100() {
		Player thePlayer = new Player();
		int result = thePlayer.getHealthCredits();
		assertEquals(100, result);
	}
	
	/**
	 * test setHealthCredit method returns 50 when set to 50
	 */
	@Test
	public void testPlayerSetHealthCreditsWith50() {
		Player thePlayer = new Player();
		thePlayer.setHealthCredits(50);
		int result = thePlayer.getHealthCredits();
		assertEquals(50, result);
	}

}
