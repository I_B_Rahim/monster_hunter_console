
package edu.westga.cs6312.monsters.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6312.monsters.model.Player;

/**
 * This is to test the fight method of the Player class
 * 
 * @author Ibrahim Tonifarah
 * @version 2/14/2019
 *
 */
class WhenPlayerFights {

	/**
	 * test fight method to see if it returns 50
	 */
	@Test
	public void testPlayerFightInflicts50Damage() {
		Player thePlayer = new Player();
		int result = thePlayer.fight();
		assertEquals(50, result);
	}

}
