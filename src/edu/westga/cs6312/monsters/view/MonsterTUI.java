
package edu.westga.cs6312.monsters.view;

import java.util.Scanner;

import edu.westga.cs6312.monsters.model.GameBoard;
import edu.westga.cs6312.monsters.model.Participant;
import edu.westga.cs6312.monsters.model.Player;

/**
 * This is creates the class for the monster user interface
 * 
 * @author Ibrahim Tonifarah
 * @version 2/14/2019
 *
 */
public class MonsterTUI {
	private GameBoard theGameBoard;
	private Scanner scanUserInput;

	/**
	 * Allows to create a MonsterTUI and instantiates instance variables
	 * 
	 * @param theGameBoard this is the game Board that establishes rooms, player and
	 *                     current location
	 */
	public MonsterTUI(GameBoard theGameBoard) {
		this.theGameBoard = theGameBoard;
		this.scanUserInput = new Scanner(System.in);
	}

	/**
	 * This serves as the director of the user textual user interface
	 */
	public void run() {
		System.out.println("Welcome To \"A Tour of Monsters \".");
		int menuOption = 0;
		while (menuOption != 9) {
			this.displayMenu();
			menuOption = this.getUserNumber("Please enter your choice: ");

			if (menuOption == 1) {
				this.describeRoom();
			} else if (menuOption == 2) {
				this.describePlayer();
			} else if (menuOption == 3) {
				this.describeGameBoard();
			} else if (menuOption == 4) {
				this.move();
			} else if (menuOption == 5) {
				menuOption = this.fight();
			} else if (menuOption == 9) {
				this.quitApplication();
			} else if (menuOption == 0) {
				System.out.println();
			} else {
				System.out.println("Please enter a valid choice.");
			}
		}
	}

	/**
	 * Allows player to fight monster if monster in room
	 */
	private int fight() {
		if (this.theGameBoard.getCurrentRoomDescription().getMonster() != null) {
			return this.playerFightingMonster();
		} else {
			System.out.println("No monster in this room to fight");
			return 0;
		}
	}

	/**
	 * Allows player to fight monster if player or monster health credits is above 0
	 * 
	 * @return 9 for quit menu option or 0 to continue
	 */
	private int playerFightingMonster() {
		Player readyPlayerOne = this.theGameBoard.getPlayer();
		Participant monsterInTheRoom = this.theGameBoard.getCurrentRoomDescription().getMonster();

		if (monsterInTheRoom.getHealthCredits() <= 0) {
			System.out.println();
			System.out.println("Monster is already defeated.");
		} else {
			int playerHealthCredits = readyPlayerOne.getHealthCredits() - monsterInTheRoom.fight();
			int monsterHealthCredits = monsterInTheRoom.getHealthCredits() - readyPlayerOne.fight();
			readyPlayerOne.setHealthCredits(playerHealthCredits);
			monsterInTheRoom.setHealthCredits(monsterHealthCredits);
			System.out.println();
			System.out.printf("The monster now has: %d remaining \n", monsterInTheRoom.getHealthCredits());
			System.out.printf("The player now has: %d remaining \n", readyPlayerOne.getHealthCredits());
		}
		if (readyPlayerOne.getHealthCredits() <= 0) {
			System.out.println();
			System.out.println("Player can no longer continue.");
			this.quitApplication();
			return 9;
		} else {
			return 0;
		}
	}

	/**
	 * Displays the menu options
	 */
	private void displayMenu() {
		System.out.println();
		System.out.println("\t1 - Describe room");
		System.out.println("\t2 - Describe player");
		System.out.println("\t3 - Describe game board");
		System.out.println("\t4 - Move");
		System.out.println("\t5 - Fight");
		System.out.println("\t9 - Quit");
	}

	/**
	 * Quits the application
	 */
	private void quitApplication() {
		System.out.println();
		System.out.printf("The player ended the game with %d health credits remaining \n\n",
				this.theGameBoard.getPlayer().getHealthCredits());
		System.out.println("Thank you for playing. Goodbye.");
		System.out.println();
	}

	/**
	 * Describes the game
	 */
	private void describeGameBoard() {
		System.out.println();
		System.out.println(this.theGameBoard.toString());
	}

	/**
	 * Describe the player in the game
	 */
	private void describePlayer() {
		System.out.println();
		System.out.println(
				this.theGameBoard.getPlayer().toString() + " and is located in " + this.theGameBoard.getCurrentRoom());
	}

	/**
	 * Describes the current room
	 */
	private void describeRoom() {
		System.out.println();
		System.out.println("The player is currently in " + this.theGameBoard.getCurrentRoomDescription());
	}

	/**
	 * Allows a message to be printed and returns the user option
	 * 
	 * @param message This is the message to be printed to the user describing what
	 *                number to enter
	 * @return the user option as an integer value
	 */
	private int getUserNumber(String message) {
		System.out.print("\t" + message);
		String userInput = this.scanUserInput.nextLine();
		int userOption = Integer.parseInt(userInput);
		return userOption;
	}

	/**
	 * Moves the player in the room
	 */
	private void move() {
		int userMoveOption = this.getUserNumber("\n1 - Left\n2 - Right\n\nEnter direction you'd like to move: ");
		if (userMoveOption == 1) {
			this.theGameBoard.moveLeft();
			this.describeRoom();
		} else if (userMoveOption == 2) {
			this.theGameBoard.moveRight();
			this.describeRoom();
		} else {
			System.out.println("Invalid direction number. Returning to main menu.");
		}
	}
}
