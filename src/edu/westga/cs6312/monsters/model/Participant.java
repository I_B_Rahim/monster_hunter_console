
package edu.westga.cs6312.monsters.model;

/**
 * This is to create a Participant and its functionality
 * 
 * @author Ibrahim Tonifarah
 * @version 2/14/2019
 *
 */
public abstract class Participant {
	private int healthCredits;

	/**
	 * Instantiates the instance variable healthCredits
	 * 
	 * @param healthCredits health credits left for participant
	 * 
	 *  Precondition:		healthCredits cannot be less than 0
	 *  Postcondition:		The participant has at least 0 health credits
	 */
	public Participant(int healthCredits) {
		if (healthCredits < 0) {
			throw new IllegalArgumentException("Invalid health credits");
		}
		this.healthCredits = healthCredits;
	}

	/**
	 * Returns the health credit of the participant
	 * 
	 * @return health credits
	 */
	public int getHealthCredits() {
		return this.healthCredits;
	}

	/**
	 * Allows to set the health credits of the participant
	 * 
	 * @param healthCredits health credits of the participant
	 * 	Precondition:		healthCredits >= 0
	 */
	public void setHealthCredits(int healthCredits) {
		if (healthCredits < 0) {
			healthCredits = 0;
		}
		this.healthCredits = healthCredits;
	}

	/**
	 * Returns a description of the Participant object
	 * 
	 * @return participant description
	 */
	@Override
	public String toString() {
		return String.format("The participant has %d health credits", this.healthCredits);
	}

	/**
	 * Returns number of health credits to be deducted from the participant they are
	 * fighting
	 * 
	 * @return opposing participant lost healthCredits
	 */
	public abstract int fight();
}
