
package edu.westga.cs6312.monsters.model;

/**
 * This is to create a Kicking Monster class with its functionality
 * 
 * @author Ibrahim Tonifarah
 * @version 2/14/2019
 *
 */
public class KickingMonster extends Participant {

	/**
	 * Sets up the Kicking monster with 100 health credits
	 */
	public KickingMonster() {
		super(100);
	}

	/**
	 * Returns a description of the KickingMonster
	 * 
	 * @return KickingMonster description
	 */
	@Override
	public String toString() {
		return String.format("Kicking Monster with %d health credits", this.getHealthCredits());
	}

	/**
	 * Returns damage inflicted by KickingMonster upon the participant they are fighting
	 * 
	 * @return 30 points damage
	 */
	@Override
	public int fight() {
		return 30;
	}

}
