
package edu.westga.cs6312.monsters.model;

/**
 * This creates a Punching Monster class and its functionality
 * 
 * @author Ibrahim Tonifarah
 * @version 2/15/2019
 *
 */
public class PunchingMonster extends Participant {
	
	/**
	 * Allows to create a Punching Monster and starting health credits
	 */
	public PunchingMonster() {
		super(75);
	}

	/**
	 * Returns number of health credits to be deducted from the participant they are
	 * fighting between 0 and 49
	 * 
	 * @return opposing participant lost healthCredits 
	 */
	@Override
	public int fight() {
		return (int) (Math.random() * 50);
	}
	
	/**
	 * Returns a description of the PunchingMonster
	 * 
	 * @return Punching description
	 */
	@Override
	public String toString() {
		return String.format("Punching Monster with %d health credits", this.getHealthCredits());
	}
}
