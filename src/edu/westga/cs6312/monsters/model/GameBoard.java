
package edu.westga.cs6312.monsters.model;

/**
 * This is to create a Game Board to keep track of the state of the game
 * 
 * @author Ibrahim Tonifarah
 * @version 2/14/2019
 *
 */
public class GameBoard {
	private Room[] roomsInGame;
	private Player thePlayer;
	private int currentRoomNumber;

	/**
	 * Allows to create a GameBoard object
	 */
	public GameBoard() {
		this.setUpBoard();
	}

	/**
	 * Initializes the player, the rooms, assigns rooms to each element and sets
	 * players current room number
	 */
	private void setUpBoard() {
		this.thePlayer = new Player();
		this.roomsInGame = new Room[10];
		this.currentRoomNumber = 0;
		for (int roomNumber = 0; roomNumber < this.roomsInGame.length; roomNumber++) {
			this.roomsInGame[roomNumber] = new Room(roomNumber);
		}
	}

	/**
	 * Returns the Player
	 * 
	 * @return the Player
	 */
	public Player getPlayer() {
		return this.thePlayer;
	}

	/**
	 * Returns the location of the current Room
	 * 
	 * @return currentRoom location
	 */
	public String getCurrentRoom() {
		return this.getCurrentRoomDescription().getLocation();
	}

	/**
	 * Returns current room details
	 * 
	 * @return details of the current room
	 */
	public Room getCurrentRoomDescription() {
		return this.roomsInGame[this.currentRoomNumber];
	}

	/**
	 * Returns a String representation of the GameBoard object
	 * 
	 * @return Gameboard textual description
	 */
	@Override
	public String toString() {
		String gameBoardDescription = "";
		for (Room aRoom : this.roomsInGame) {
			gameBoardDescription += aRoom.toString() + "\n";
		}
		gameBoardDescription += this.thePlayer.toString() + " and is located in "
				+ this.getCurrentRoomDescription();
		return gameBoardDescription;
	}
	
	/**
	 * Moves the current player one room to the right
	 */
	public void moveRight() {
		if (this.currentRoomNumber == 9) {
			this.currentRoomNumber = 0;
		} else {
			this.currentRoomNumber++;
		}
	}
	
	/**
	 * Moves the current player one room to the left
	 */
	public void moveLeft() {
		if (this.currentRoomNumber == 0) {
			this.currentRoomNumber = 9;
		} else {
			this.currentRoomNumber--;
		}
	}

}
