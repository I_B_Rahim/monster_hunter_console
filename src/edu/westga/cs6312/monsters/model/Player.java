
package edu.westga.cs6312.monsters.model;

/**
 * This is to create a player and its functionality
 * 
 * @author Ibrahim Tonifarah
 * @version 2/14/2019
 *
 */
public class Player extends Participant {

	/**
	 * Sets up the player object with 100 health credits
	 */
	public Player() {
		super(100);
	}

	/**
	 * Returns a description of the player object
	 * 
	 * @return Player description
	 */
	@Override
	public String toString() {
		return String.format("The player has %d health credits", this.getHealthCredits());
	}

	/**
	 * Returns damage inflicted by player upon the participant they are fighting
	 * 
	 * @return 50 points damage
	 */
	@Override
	public int fight() {
		return 50;
	}
}
