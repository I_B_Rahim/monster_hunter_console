
package edu.westga.cs6312.monsters.model;

/**
 * This is to create a room to keep track of an individual room in the game
 * 
 * @author Ibrahim Tonifarah
 * @version 2/14/2019
 *
 */
public class Room {
	private int roomIDNumber;
	private Participant monster;

	/**
	 * Instantiates the instance variables
	 * 
	 * @param roomIDNumber Identifying number of the room
	 * 
	 *                     Precondition: roomIDNumber cannot be less than 0
	 *                     Postcondition: roomIDNumber >= 0
	 */
	public Room(int roomIDNumber) {
		if (roomIDNumber < 0) {
			throw new IllegalArgumentException("Invalid Room Number");
		}
		this.roomIDNumber = roomIDNumber;
		this.setUpRoom();
	}

	/**
	 * Randomly determines if a monster should be added to a room with a 50% chance.
	 */
	private void setUpRoom() {
		if (this.randomChance() == 1) {
			this.createMonster();
		} else {
			this.monster = null;
		}
	}

	/**
	 * Creates a new monster 
	 * 
	 * @return a Kicking or Punching Monster
	 */
	private void createMonster() {
		if (this.randomChance() == 0) {
			KickingMonster aRoomKickMonster = new KickingMonster();
			this.monster = aRoomKickMonster;
		} else {
			PunchingMonster aRoomPunchMonster = new PunchingMonster();
			this.monster = aRoomPunchMonster;
		}
	}

	/**
	 * Returns either 0 or 1 as a random number
	 * 
	 * @return 0 or 1
	 */
	private int randomChance() {
		int randomNumber = (int) (Math.random() * 2);
		return randomNumber;
	}

	/**
	 * Returns the location of the room based on ID number
	 * 
	 * @return room ID number
	 */
	public String getLocation() {
		return String.format("Room at (%d)", this.roomIDNumber);
	}

	/**
	 * Gets the monster in the Room
	 * 
	 * @return the monster
	 */
	public Participant getMonster() {
		return this.monster;
	}

	/**
	 * Returns a description of Monster
	 * 
	 * @return string describing monster in room
	 */
	private String monsterInRoom() {
		if (this.getMonster() == null) {
			return "no Monster inside";
		} else {
			return this.getMonster().toString();
		}
	}

	/**
	 * Returns a description of the Room object
	 * 
	 * @return Description of the Room
	 */
	@Override
	public String toString() {
		return String.format("%s with %s", this.getLocation(), this.monsterInRoom());
	}
}
