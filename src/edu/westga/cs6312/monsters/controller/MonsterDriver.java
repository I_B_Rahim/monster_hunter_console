
package edu.westga.cs6312.monsters.controller;

import edu.westga.cs6312.monsters.model.GameBoard;
import edu.westga.cs6312.monsters.view.MonsterTUI;

/**
 * This is the driver for the MonsterTUI application
 * 
 * @author Ibrahim Tonifarah
 * @version 2/14/2019
 *
 */
public class MonsterDriver {

	/**
	 * Entry into the Application
	 * 
	 * @param args Not used
	 */
	public static void main(String[] args) {
		GameBoard theGameBoard = new GameBoard();
		MonsterTUI theMonsterApplication = new MonsterTUI(theGameBoard);
		theMonsterApplication.run();
	}
}
